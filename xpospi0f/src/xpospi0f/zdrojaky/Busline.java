
package xpospi0f.zdrojaky;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class Busline 
{
	private java.lang.String id_l;
	private List<Two_stop_path> line_path = new ArrayList<>();
      
	/**
            * Setter pro nastavení jména autobusové linky.
            * @param id název linky
        */
	public Busline(String id)
	{	
		this.id_l = id;
	}
        
        /**
            * Vrací název autobusové linky.
            * @return název autobusové linky.
        */
        public String getid()
        {
            return this.id_l;
        }
	
        /**
            * Přidá spojení mezi dvěma zastávekami do seznamu zastávek.
            * @param path spojení mezi devěmi zastávkami
        */
	public void addstop_conection(Two_stop_path path)
	{
            this.line_path.add(path);
	
	}
        
        /**
            * Vrací seznam, jehož každá položka reprezentuje spojení mezi dvěmi zastávkami.
            * @return výše popsaný seznam.
        */
        public List<Two_stop_path> get_line_path()
        {
            return this.line_path;
        }
        
        

        
        
        
        /*
        
        POSSIBLE JUNK
        
        public void setPath()
        {
            
            
            
            for(int i = 0; i < streets.size(); i++)
            {
                List<Coordinate> pomcor = streets.get(i).getCoordinates();
                
                for(int c = 0; c < pomcor.size(); c++)
                {
                    line_path.add(pomcor.get(c));
                }
                
            }
            
            line_path.set(0, streets.get(0).getStops().get(0).getCoordinate());
            
            List<Stop> pomstop = streets.get(streets.size()-1).getStops();
            
            line_path.set(line_path.size()-1, pomstop.get(pomstop.size()-1).getCoordinate());
            
        }
            
      
        
        
    public double Compute_distance()
    {
        double length = 0;
        for(int i = 0; i < this.streets.size(); i++)
        {
            
            if(this.streets.size() == 0)
            {
                length = length + getDistance(streets.get(i).begin(), streets.get(i).end())
            }
            
            
            List<Stop> pomstop = streets.get(i).getStops();
                
            for(int c = 0; c < pomstop.size(); c++)
            {

            }
            Coordinate c1 = this.line_path.get(i);
            Coordinate c2 = this.line_path.get(i+1);
            
            length = length + getDistance(c1, c2);
        }
       
        return length;
    }
    public double getDistance(Coordinate c1, Coordinate c2)
    {
        return Math.sqrt(Math.pow(c1.getX() - c2.getX(), 2) + Math.pow(c1.getY() - c2.getY(), 2));
    }

	
	public boolean addStreet(Street street)
	{
		if(this.street_stop.size() != 0)
		{
			if(this.street_stop.get(this.street_stop.size()-1).getKey().follows(street))
			{
				this.street_stop.add(new java.util.AbstractMap.SimpleImmutableEntry<Street,Stop>(street,null));
				return true;
			}
		}
		
		return false;
	}
	
	

   
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
