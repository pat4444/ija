/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xpospi0f;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import xpospi0f.zdrojaky.Busline;
import xpospi0f.zdrojaky.Coordinate;
import xpospi0f.zdrojaky.Drawable;
import xpospi0f.zdrojaky.Path;
import xpospi0f.zdrojaky.Stop;
import xpospi0f.zdrojaky.Street;
import xpospi0f.zdrojaky.Two_stop_path;
import xpospi0f.zdrojaky.Vehicle;

import javax.crypto.Mac;

/**
 *
 * @author pat4444
 * TODO
 * xpospi0f:
 * doplnit vsechny spoje mezi ulicemi
 * vice autobusu
 * doplnit jak dlouho ktery bus kam jede a brat to v uvahu
 * print_busline se nepřepisuje po zadání jiného busu!
 */
public class Main extends Application {
    private static List<String> cislaLinek_vypis = new ArrayList<>();
    private static List<String> zastavky_vypis = new ArrayList<>();
    private static List<String> seznam_busu = new ArrayList<>();
    private static List<Vehicle> seznam_busu_symlink = new ArrayList<>();
    private static List<String> zastavky_linky = new ArrayList<>();
    private static List<String> seznam_spoju = new ArrayList<>();
    private static List<String> seznam_linek = new ArrayList<>();
    private static List<Busline> seznam_linek_symlink = new ArrayList<>();
    private static List<String> seznam_casu_odjezdu = new ArrayList<>();
    public static List<Drawable> elem = new ArrayList<>();
    public static List<String> seznamZastavek = new ArrayList<>();
    public static List<Stop> symlinkNaZastavky = new ArrayList();
    public static List<Street> seznam_ulic = new ArrayList<>();

    public static List<String> vypis_zastavek(){
        return zastavky_vypis;
    }

    public static List<String> vypis_linek(){
        return cislaLinek_vypis;
    }

    public static List<String> get_seznam_busu(){
        return seznam_busu;
    }

    public static List<Vehicle> get_seznam_busu_symlink(){
        return seznam_busu_symlink;
    }

    public static List<String> get_zastavky_linky(){
        return zastavky_linky;
    }

    public static List<String> get_seznam_spoju(){
        return seznam_spoju;
    }

    public static List<String> get_seznam_linek(){
        return seznam_linek;
    }

    public static List<String> get_seznam_casu_odjezdu(){
        return seznam_casu_odjezdu;
    }

    public static List<String> get_seznamZastavek(){
        return seznamZastavek;
    }

    public static List<Stop> get_symlinkNaZastavky(){
        return symlinkNaZastavky;
    }
    
    public static List<Street> get_seznam_ulic(){
        return seznam_ulic;
    }

    public static List<Busline> get_seznam_linek_symlink(){
        return seznam_linek_symlink;
    }

    public static List<Drawable> get_elements(){
        return elem;
    }
    
    

    @Override
    public void start(Stage primaryStage) throws Exception{
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("layout.fxml"));
        BorderPane root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        MainController controller = loader.getController();
        

        
//        Vehicle bus1 = new Vehicle(new Coordinate(200, 200), 50 );
        
        //Vehicle bus1 = new Vehicle(new Coordinate(100, 100), 50);
  //      Vehicle bus2 = new Vehicle(new Coordinate(150, 200), 20);
        List<String> seznamUlic = new ArrayList<>();
        List<Street> symlinkNaUlice = new ArrayList();
        List<String> nazevCest = new ArrayList<>();

        List<Two_stop_path> symlinkNaNazvy = new ArrayList<>();

        java.nio.file.Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        
        
//        NAČÍTÁNÍ ULIC
        try {
            File myObj = new File(s+"\\src\\xpospi0f\\soubory_vstupni\\ulice_jednoduche.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String str = data.toString();
                String[] arrOfStr = str.split(":");

                String ulice = arrOfStr[0];
                String souradnice_pocatek = arrOfStr[1];
                String[] split_zacatek = souradnice_pocatek.split(",");
                String souradnice_konec = arrOfStr[2];
                String[] split_konec = souradnice_konec.split(",");

                seznamUlic.add(ulice);
                Street ulice1 = new Street(ulice, new Coordinate(Double.valueOf(split_zacatek[0]),Double.valueOf(split_zacatek[1])), new Coordinate(Double.valueOf(split_konec[0]),Double.valueOf(split_konec[1])));
                seznam_ulic.add(ulice1);
                symlinkNaUlice.add(ulice1);
                elem.add(ulice1);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Chyba pri otevirani souboru s mapami!");
            e.printStackTrace();
        }

        //        NAČÍTÁNÍ Zastavek
        try {
            File myObj = new File(s +"\\src\\xpospi0f\\soubory_vstupni\\zastavky.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String str = data.toString();
                String[] arrOfStr = str.split(":");

                String zastavka = arrOfStr[0];
                String souradnice_zastavky = arrOfStr[1];
                String[] split_souradnice = souradnice_zastavky.split(",");
                String naUlici = arrOfStr[2];

                int poradi = 0;
                while (poradi < seznamUlic.size()){
                    if(seznamUlic.get(poradi).toString().equals(naUlici.toString())){
                        seznamZastavek.add(zastavka);
                        Stop zastavka1 = new Stop(zastavka, new Coordinate(Double.valueOf(split_souradnice[0]),Double.valueOf(split_souradnice[1])), symlinkNaUlice.get(poradi));
                        symlinkNaZastavky.add(zastavka1);
                        elem.add(zastavka1);
                    }
                    poradi = poradi + 1;
                }

            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Chyba pri otevirani souboru se zastavkami!");
            e.printStackTrace();
        }

        //Načitání propojení zastávek:
        try {
            File myObj = new File(s+"\\src\\xpospi0f\\soubory_vstupni\\propojeni_zastavek.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] arrOfStr = data.split(":");
                String nazev = arrOfStr[0]+arrOfStr[1];
                String pocatek = arrOfStr[0];
                String konec = arrOfStr[1];
                Stop pocatekStop = null;
                Stop konecStop = null;
                for (int i = 0; i < seznamZastavek.size(); i++) {
                    if(seznamZastavek.get(i).toString().equals(pocatek.toString())){
                        pocatekStop = symlinkNaZastavky.get(i);
                    }
                    else if(seznamZastavek.get(i).toString().equals(konec.toString())){
                        konecStop = symlinkNaZastavky.get(i);
                    }
                }
                nazevCest.add(nazev);
                Two_stop_path path1 = new Two_stop_path(pocatekStop,konecStop);
                symlinkNaNazvy.add(path1);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Chyba pri otevirani souboru se propojenim zastavek!");
            e.printStackTrace();
        }

        try {
            File myObj = new File(s+"\\src\\xpospi0f\\soubory_vstupni\\autobusy_se_zastavkami.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] arrOfStr = data.split(":");
                String cislo_linky = arrOfStr[0];
                String[] souradnice = arrOfStr[1].split(",");
                String rychlost = arrOfStr[2];
                String[] cesta = arrOfStr[3].split(",");
                String[] zastavky = arrOfStr[4].split(",");
                Busline linka1 = new Busline(arrOfStr[0]);
                String zastavky_linka = arrOfStr[0]+":"+arrOfStr[4];
                zastavky_linky.add(zastavky_linka);
                seznam_linek.add(cislo_linky);
                seznam_linek_symlink.add(linka1);
                for (int i = 0; i < cesta.length; i++) {
                    for (int n = 0; n < nazevCest.size(); n++) {
                        if (nazevCest.get(n).toString().equals(cesta[i].toString())) {
                            linka1.addstop_conection(symlinkNaNazvy.get(n));
                        }
                    }
                }
                }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Chyba pri otevirani souboru s autobusy!");
            e.printStackTrace();
        }

  //      výpisy linek
        try {
            File myObj = new File(s+"\\src\\xpospi0f\\soubory_vstupni\\autobusy_se_zastavkami.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] arrOfStr = data.split(":");
                String vystup = "";

                cislaLinek_vypis.add(arrOfStr[0]);
                String[] zastavky_naout =  arrOfStr[4].split(",")   ;
                for (int i = 0; i < zastavky_naout.length; i++) {
                    if (i == 0){
                        vystup = vystup + zastavky_naout[i];
                    }
                    else{
                        vystup = vystup + ":" + zastavky_naout[i];
                    }
                }
                zastavky_vypis.add(vystup);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Chyba pri otevirani souboru s autobusy!");
            e.printStackTrace();
        }

        //cteni spoju
        try {
            File myObj = new File(s+"\\src\\xpospi0f\\soubory_vstupni\\spoje.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                seznam_spoju.add(data);
                String[] arrOfStr = data.split(",");
                String linka = arrOfStr[0];
                String cas = arrOfStr[1];
                seznam_busu.add(linka);
                Vehicle bus1 = new Vehicle(new Coordinate(Double.valueOf(0), Double.valueOf(0)), Double.valueOf(0));
                seznam_busu_symlink.add(bus1);
                seznam_casu_odjezdu.add(cas);
                for (int i = 0; i < seznam_linek.size(); i++){
                    if (linka.equals(seznam_linek.get(i))){
                        bus1.set_vehicle_busline(seznam_linek_symlink.get(i));
                        bus1.set_vehicle_path();
                        elem.add(bus1);
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Chyba pri otevirani souboru se spoji!");
            e.printStackTrace();
        }


//        Stop s1 = new Stop("s1", new Coordinate(200, 200), novakova);
//        Stop s2 = new Stop("s2", new Coordinate(300, 300), cervena);
//        Stop s3 = new Stop("s3", new Coordinate(400, 100), karlova);
//
//        Two_stop_path path1 = new Two_stop_path(s1,s2);
//        Two_stop_path path2 = new Two_stop_path(s2,s3);
//
//        Busline linka1 = new Busline("1");
//        linka1.addstop_conection(path1);
//        linka1.addstop_conection(path2);
//
//
//        //new Path(Arrays.asList(new Coordinate(200, 200), new Coordinate(300, 300), new Coordinate(500, 150), new Coordinate(400, 100)))
//        bus1.set_vehicle_busline(linka1);
//        bus1.set_vehicle_path();
//
//        bus2.set_vehicle_busline(linka1);
//        bus2.set_vehicle_path();
//
//
//        elem.add(bus1);
//        elem.add(bus2);
////        elem.add(novakova);
////        elem.add(cervena);
////        elem.add(karlova);
//        elem.add(s1);
//        elem.add(s2);
//        elem.add(s3);
        
        
        
        
        //elem.add(new Vehicle(new Coordinate(100, 100), 50, new Path(Arrays.asList(new Coordinate(100,100),new Coordinate(100,100),new Coordinate(160,300),new Coordinate(80,180) ,new Coordinate(500,500) ))));

        
        
        controller.setElements(elem);
        
        controller.start(1);
        
        
        /*
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        */
    }

    /*
    public static void main(String[] args) {
        launch(args);
    }
    */
}

